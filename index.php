
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Task</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="js/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="js/slick-1.8.1/slick/slick-theme.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/media.css">

</head>
<body>

<header>
    <div class="wrapper-center">
        <nav class="nav-bar__nav">
            <ul class="nav-list__ul">
                <li>О нас</li>
                <li>Новое поступление</li>
                <li>Доставка и оплата</li>
                <li>Прайс-листы</li>
                <li>Сотрудничество</li>
                <li>Контакты</li>
            </ul>
            <ul class="nav-list-mobile">
                <li class="burger-nav-number">Menu</li>
                <li>О нас</li>
                <li>Новое поступление</li>
                <li>Доставка и оплата</li>
                <li>Прайс-листы</li>
                <li>Сотрудничество</li>
                <li>Контакты</li>
            </ul>
            <span class="log-reg__span">
                <a href="ordredProduct.php">Вход</a>
                <i>/</i>
                <a href="#">Регестрация</a>
            </span>
        </nav>
    </div>

    <hr class="gray-horiz-line__hr">

    <div class="wrapper-center">
        <div class="logo-cart__div">
            <div class="logo-description">
                <img class="main-logo" src="img/logo_radio_store.png" alt="logo_radio_store">

                <span class="header-description__span">
                    Электронные компоненты и радиодетали, источники притания и LED продукция
                </span>
            </div>


            <div class="contact-cart__div">

                <div class="ph-numbers__div">
                    <div class="working-hr__div">
                        <img src="img/icons/clock_icon.png" alt="clock-icon">
                        Пн -Пт 9:30 по 18:30
                    </div>
                    <a class="main-ph-number__a" href="tel: +380965976862">096 597 68 62</a>
                    <ul class="ph-number-list__ul">
                        <li class="name-number-list">Все телефоны</li>
                        <li>096 597 68 62</li>
                        <li>096 597 68 62</li>
                        <li>096 597 68 62</li>
                        <li>096 597 68 62</li>
                    </ul>
                </div>

                <div class="address__div">
                    <ul class="our-address-list__ul">
                        <li class="name-address-list">Наши адреса</li>
                    </ul>
                    <a class="call-back__a" href="#">Обратный звонок</a>
                </div>

                <div class="cart">
                    <div class="cart-logo-amount">
                        <span class="cart-amount">0</span>
                        <img src="img/icons/cart_icon.png" alt="cart-icon">
                    </div>

                    <div class="cart-name-price">
                        <div class="cart-name">Корзина</div>
                        <div class="cart-price">0 грн.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="gray-horiz-line__hr light-gray-hr-line">

    <div class="wrapper-center">
        <div class="catalog-search">
            <ul class="catalog">
                <li class="catalog-name">
                    <div class="burger-btn">
                        <div class="burger-lines"></div>
                    </div>
                    <span class="catalog-name-text">Каталог товаров</span>
                </li>
                <li class="catalog-item">
                    <span class="catalog-number">1</span>
                    <span>Паяльники</span>
                </li>
                <li class="catalog-item">
                    <span class="catalog-number">2</span>
                    <span>Проводники</span>
                </li>
                <li class="catalog-item">
                    <span class="catalog-number">3</span>
                    <span>Лампочки</span>
                </li>
            </ul>

            <input class="search-area" type="text" placeholder="Поиск в каталоге">
            <img class="search-icon" src="img/icons/search-icon.png" alt="">
        </div>
    </div>

    <hr class="gray-horiz-line__hr light-gray-hr-line">
</header>

<main>
    <section class="adv-slider">
        <div class="adv-slider-images">
            <a href="#"><img src="img/banner.png" alt="adv"></a>
            <a href="#"><img src="img/banner.png" alt="adv"></a>
            <a href="#"><img src="img/banner.png" alt="adv"></a>
            <a href="#"><img src="img/banner.png" alt="adv"></a>
        </div>
        <i class="slider-nxt-btn"></i>
        <i class="slider-prev-btn"></i>
        <!--        тут можно улучшить кнопки-->
    </section>

    <section class="categories wrapper-center">
        <div class="categories-item promotional-stuff">
            <span class="ctg-item-name">Акионные товары
            <button class="ctg-item-btn ctg-item-btnmob">Подробнее</button>
            </span>
            <hr class="ctg-item-line">
            <p class="ctg-item-text">
                Поступление нового товара за
                последние 7 календарных дней.
                Лучшие цены в Украине
            </p>
            <button class="ctg-item-btn">Подробнее</button>
        </div>
        <div class="categories-item catalog-stuff">
            <span class="ctg-item-name">Акионные товары
            <button class="ctg-item-btn ctg-item-btnmob">Подробнее</button>
            </span>
            <hr class="ctg-item-line">
            <p class="ctg-item-text">
                Поступление нового товара за
                последние 7 календарных дней.
                Лучшие цены в Украине
            </p>
            <button class="ctg-item-btn">Подробнее</button>
        </div>
        <div class="categories-item new-arrivals">
            <span class="ctg-item-name">Акионные товары
            <button class="ctg-item-btn ctg-item-btnmob">Подробнее</button>
            </span>
            <hr class="ctg-item-line">
            <p class="ctg-item-text">
                Поступление нового товара за
                последние 7 календарных дней.
                Лучшие цены в Украине
            </p>
            <button class="ctg-item-btn">Подробнее</button>
        </div>
    </section>

    <section class="best-deal wrapper-center">
        <header class="best-deal-header">
            <span class="best-deal-name">Лучшее предложения</span>
            <hr class="best-deal-hline">
            <div>
                <button class="best-deal-slider-prev-btn"></button>
                <button class="best-deal-slider-nxt-btn"></button>
            </div>
        </header>

        <div class="best-deal-slider">
            <?php require('php/fetch-product.php'); ?>
        </div>
    </section>

    <section class="info-section wrapper-center">
        <div class="info-about-us">
            <span class="about-us-title">O нас</span>
            <hr class="green-info-line">
            <div class="about-us-text">
                <p>
                    Мы являемся современной, быстро развивающейся компанией, в которой основными принципами работы
                    являются высокий профессионализм, ответственность перед каждым клиентом и надежность гарантируемого
                    результата.
                </p>
                <p>
                    В жизнь эти принципы воплощаются командой высококвалифицированных специалистов, которые работают на
                    благо компании и разделяют общие ценности и цели.
                </p>

                <a class="about-us-more-info" href="#">Поробнее <img src="img/icons/little_green_arrow.png" alt="arrow"></a>
            </div>
        </div>

        <div class="info-product-day">
            <span class="product-day-title">Товар Дня</span>
            <hr class="green-info-line">
            <div class="product-day-wrapper">
                <div class="product-day-item little-store-item">
                    <img class="little-store-item-img" src="img/store_item.png" alt="store-item">
                    <div>
                    <span class="store-item-name">
                        Автомобильный блок питания для
                    ноутбука ASUS W5G00A 19V 3.42
                    A 5.5x2.5
                    </span>
                        <div class="store-item-info little-item-info">
                            <div class="info-delivery-amount">
                        <span class="info-delivery">
                            <img src="img/icons/car_icon.png" alt="car-icon">
                            Есть доставка
                        </span>
                                <span class="info-amount">
                            <img src="img/icons/checked.png" alt="checked-icon">
                            В наличии 23 шт.
                        </span>
                            </div>
                            <span class="info-price">
                        188,40
                    </span>
                        </div>
                    </div>
                </div>
                <div class="product-day-item little-store-item">
                    <img class="little-store-item-img" src="img/store_item.png" alt="store-item">
                    <div>
                    <span class="store-item-name">
                        Автомобильный блок питания для
                    ноутбука ASUS W5G00A 19V 3.42
                    A 5.5x2.5
                    </span>
                        <div class="store-item-info little-item-info">
                            <div class="info-delivery-amount">
                        <span class="info-delivery">
                            <img src="img/icons/car_icon.png" alt="car-icon">
                            Есть доставка
                        </span>
                                <span class="info-amount">
                            <img src="img/icons/checked.png" alt="checked-icon">
                            В наличии 23 шт.
                        </span>
                            </div>
                            <span class="info-price">
                        188,40
                    </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="info-utility">
            <span class="utility-title">Полезности</span>
            <hr class="green-info-line">
            <div class="info-utility-wrapper">
                <div class="utility-item">
                <span class="utility-date">
                    15
                    <span class="utility-date-month">Декабря</span>
                </span>
                    <div class="utility-info">
                    <span class="utility-info-title">
                        Обзор паяльной станции
                        Yihua 898BD+ 2 в 1
                    </span>
                        <span class="utility-info-description">
                        Представляем Вашему вниманию
                        видеообзор паяльнойстанции ...
                    </span>
                    </div>
                </div>
                <div class="utility-item">
                <span class="utility-date">
                    15
                    <span class="utility-date-month">Декабря</span>
                </span>
                    <div class="utility-info">
                    <span class="utility-info-title">
                        Обзор паяльной станции
                        Yihua 898BD+ 2 в 1
                    </span>
                        <span class="utility-info-description">
                        Представляем Вашему вниманию
                        видеообзор паяльнойстанции ...
                    </span>
                    </div>
                </div>
                <div class="utility-item">
                <span class="utility-date">
                    15
                    <span class="utility-date-month">Декабря</span>
                </span>
                    <div class="utility-info">
                    <span class="utility-info-title">
                        Обзор паяльной станции
                        Yihua 898BD+ 2 в 1
                    </span>
                        <span class="utility-info-description">
                        Представляем Вашему вниманию
                        видеообзор паяльнойстанции ...
                    </span>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>

<footer>
    <div class="footer-info">
        <div class="wrapper-center strip-info">
            <div class="strip-info-item ft-new-arrivals">
                <img class="strip-info-img" src="img/icons/cart-with-stuff.png" alt="stuff-cart">
                <div class="strip-info-title-descr">
                    <span class="strip-info-tittle">Новые поступления</span>
                    <span class="strip-info-description"> Быстрая доставка по всей Украине</span>
                </div>
            </div>
            <div class="strip-info-item ft-new-arrivals">
                <img class="strip-info-img" src="img/icons/riding_car_icon.png" alt="stuff-cart">
                <div class="strip-info-title-descr">
                    <span class="strip-info-tittle">Оплата и доставка</span>
                    <span class="strip-info-description"> Быстрая доставка по всей Украине</span>
                </div>
            </div>
            <div class="strip-info-item ft-new-arrivals">
                <img class="strip-info-img" src="img/icons/document_icon.png" alt="stuff-cart">
                <div class="strip-info-title-descr">
                    <span class="strip-info-tittle">Наши прайс-листы</span>
                    <span class="strip-info-description"> Быстрая доставка по всей Украине</span>
                </div>
            </div>
            <div class="strip-info-item ft-new-arrivals">
                <img class="strip-info-img" src="img/icons/place_on_map_icon.png" alt="stuff-cart">
                <div class="strip-info-title-descr">
                    <span class="strip-info-tittle">Наши магазины</span>
                    <span class="strip-info-description"> Быстрая доставка по всей Украине</span>
                </div>
            </div>
        </div>

    </div>

    <div class="wrap-nav-footer">
        <nav class="nav-footer wrapper-center">
            <ul class="nav-about-comp">
                <li>О компании</li>
                <li><a href="#">Новое поступление</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Дпропшопинг с нами</a></li>
                <li><a href="#">Сотрудничество с предприятиями</a></li>
            </ul>
            <ul class="nav-service">
                <li>Сервис</li>
                <li><a href="#">Доставка и оплата</a></li>
                <li><a href="#">Наи прайс-листы</a></li>
                <li><a href="#">Каталог товаров</a></li>
            </ul>
            <ul class="nav-call-us">
                <li>Звоните нам</li>
                <li>
                    <a class="call-us-number" href="tel: +380965976862">096 597-68-62</a>
                    <div class="number-adr">пр. Правды 83/3</div>
                </li>
                <li>
                    <a class="call-us-number" href="tel: +380971140918">097 114-09-18</a>
                    <div class="number-adr">пр. Д. Яворницкого, 121 </div>
                </li>
                <li>
                    <a class="call-us-number" href="tel: +380957946582">095 794-65-82</a>
                    <div class="number-adr">пр. Слобожанский, 83 </div>
                </li>
            </ul>
            <ul class="nav-social-copyright">
                <li>Мы в соцсетях</li>
                <li class="social-network">
                    <img src="img/icons/vk-icon.png" alt="vk-icon">
                    <img src="img/icons/facebook-icon.png" alt="facebook-icon">
                    <img src="img/icons/classmates_icon.png" alt="classmates-icon">
                </li>
                <li class="copyright">
                    © <?php echo date(o)?> Radio Store<br>
                    Разработка сайта <a href="#">First Ukrainian Studio</a>
                </li>
            </ul>
        </nav>
    </div>

</footer>

<section class="order-product">
    <div class="order-window">
        <hr class="green-info-line">
        <div class="order-wrapper">
            <div class="close-window"></div>
            <form id="make-order" class="customer-form" action="">
                <fieldset>
                    <label class="order-window-title"> Оформление заказа </label>
                    <input type="text"
                           name="name"
                           placeholder="Имя"
                           required>
                    <input type="tel"
                           name="phonenumber"
                           pattern="[3-7]{1}[0-9]{11}"
                           placeholder="380 (___)__-__"
                           required>
                    <input type="email"
                           name="email"
                           placeholder="example@email.com"
                           required>
                </fieldset>
            </form>

            <div class="list-selected-items">

            </div>

            <div class="price-and-buy">
                <span class="total-price">Итог: <span class="price-value">0</span> грн.</span>
                <input class="sbm-btn" form="make-order" type="submit" value="оформить заказ">
            </div>

        </div>

    </div>
</section>

</body>

<script src="js/jquery.min.js"></script>
<script src="js/slick-1.8.1/slick/slick.js"></script>
<script src="js/main.js"></script>
</html>