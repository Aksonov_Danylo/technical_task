<?php

$item_new = '<div idProd="1" class="best-deal-item store-item new-store-item">
                <img class="store-item-img" src="img/store_item.png" alt="item">
                <span class="store-item-name">
                    Автомобильный блок питания для
                    ноутбука ASUS W5G00A 19V 3.42
                    A 5.5x2.5
                </span>
                <div class="store-item-info">
                    <div class="info-delivery-amount">
                        <span class="info-delivery">
                            <img src="img/icons/car_icon.png" alt="car-icon">
                            Есть доставка
                        </span>
                        <span class="info-amount">
                            <img src="img/icons/checked.png" alt="checked-icon">
                            В наличии 23 шт.
                        </span>
                    </div>
                    <span class="info-price">
                        138,40
                    </span>
                </div>
                <div class="buy-order">
                    <button class="buy-btn" value="1" priceItem="138.40">Купить</button>
                    <a class="order-link" href="#">Заказать в <b class="red-number">1</b> клик</a>
                </div>

            </div>';
$item_stock = '<div idProd="2" class="best-deal-item store-item stock-store-item">
                <img class="store-item-img" src="img/store_item.png" alt="item">
                <span class="store-item-name">
                    Автомобильный блок питания для
                    ноутбука ASUS W5G00A 19V 3.42
                    A 5.5x2.5
                </span>
                <div class="store-item-info">
                    <div class="info-delivery-amount">
                        <span class="info-delivery">
                            <img src="img/icons/car_icon.png" alt="car-icon">
                            Есть доставка
                        </span>
                        <span class="info-amount">
                            <img src="img/icons/checked.png" alt="checked-icon">
                            В наличии 23 шт.
                        </span>
                    </div>
                    <span class="info-price">
                        188,40
                    </span>
                </div>
                <div class="buy-order">
                    <button class="buy-btn" value="2" priceItem="188.4">Купить</button>
                    <a class="order-link" href="#">Заказать в <b class="red-number">1</b> клик</a>
                </div>

            </div>';

$arr = array($item_new, $item_stock, $item_new, $item_stock, $item_stock, $item_new, $item_stock);
foreach ($arr as &$value) {
    echo $value;
}
unset($value);

?>
