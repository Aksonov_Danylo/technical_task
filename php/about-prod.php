<?php

$name_1 = 'Автомобильный блок питания для ноутбука ASUS W5G00A 19V 3.42 A 5.5x2.5';
$name_2 = 'Автомобильный блок питания для ноутбука ASUS W5G00A 19V 3.42 A 5.5x2.5';

$price_1 = 138.40;
$price_2 = 188.40;

$item_1 = "<div class='little-store-item ordered-item'>
                <img class='little-store-item-img' src='img/store_item.png' alt='store-item'>
                <div>
                    <span class='store-item-name'>
                        $name_1
                    </span>
                    <div class='store-item-info little-item-info'>
                        <span class='info-price'> $price_1 </span>
                    </div>
                </div>
            </div>";
$item_2 = "<div class='little-store-item ordered-item'>
                <img class='little-store-item-img' src='img/store_item.png' alt='store-item'>
                <div>
                    <span class='store-item-name'>
                        $name_2
                    </span>
                    <div class='store-item-info little-item-info'>
                        <span class='info-price'> $price_2 </span>
                    </div>
                </div>
            </div>";