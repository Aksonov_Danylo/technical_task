$('.adv-slider-images').slick({
    dots: false,
    infinite: true,
    speed: 400,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
});

$('.slider-nxt-btn').click(() => {
    $('.adv-slider-images').slick('slickNext');
});

$('.slider-prev-btn').click(() => {
    $('.adv-slider-images').slick('slickPrev');
});


$('.best-deal-slider').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
        {
            breakpoint: 1400,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerPadding: '100px'
            }
        }

    ]
});

$('.best-deal-slider-nxt-btn').click(() => {
    $('.best-deal-slider').slick('slickNext');
});

$('.best-deal-slider-prev-btn').click(() => {
    $('.best-deal-slider').slick('slickPrev');
});

window.radioStore = {
    //ф-ция инициализации и обновления данных, в которой создаеться объект с проверочными данными
    initial: function () {
        let verificationData;
        if(!verificationData){ verificationData = this.review(); }

        console.log(verificationData);

        verificationData.cartItems && this.updateCartAmount();
        verificationData.priceItemArr && this.updateTotalPrice();
    },

    //ф-ция которая делает проверку и возвращает объект с данными проверки
    review: function(){
        let cartItems = localStorage.getItem('idItem');
        let priceItemArr = localStorage.getItem('prices');

        let check = (val) => !!val;

        return{
            cartItems: check(cartItems),
            priceItemArr: check(priceItemArr)
        };
    },

    //ф-ция которая обрабатывает отрисовку выпадающего меню
    dropdown: function(parent) {

        let children = parent.children();
        let heightItem = children[0].offsetHeight;

        //в строке 44 изменяем координаты кждого элемента списка, путем умножения его индекса на его высоту,
        //высоту элемента получаем в строке 41

        let changeHeight = (index, item, mult) => {
            (index !== 0) && $(item).css('top',`${index * heightItem * mult}px`)
        };

        parent.toggleClass('active');

        parent.hasClass('active')
            ? children.each((index, item) => changeHeight(index, item, 1))
            : children.each((index, item) => changeHeight(index, item, 0))
        //параметр mult - множитель, в случае если координаты элемента нужно увеличить, передаем 1, т.к
        // 1 не повиляет на измениния значения top в строке 44

    },

    //ф-ция обрабатывает отрисовку корзины
    openCart: function(parent){

        let body = $('body');
        let orderPopup = $('.order-product');

        parent.toggleClass('active');
        body.toggleClass('body-fixed');
        orderPopup.toggleClass('active');

    },

    //ф-ция обработки заказа
    makeOrder: async function(event){
        event.preventDefault();
        let promise = await fetch('php/sendData.php',{
            method: 'POST',
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            //отправляем данные формы и массив выбраных товаров
            body: `${$(this).serialize()}&arrayOfIdItems=${localStorage.getItem('idItem')}`
        });

        if(promise.ok){
            console.log(await promise.text())
        }
    },

    //ф-ция отправки значений выбраных товаров в LS, принимает:
    //ключ - по какому записывать, строка
    //значение - которое записывать, строка
    itemToLocalStorage: function(key, strValue){
        let value = Number(strValue);
        let str = localStorage.getItem(key);

        //ф-ция записи
        let setItem = (arr = []) => {
            let items = JSON.stringify([...arr, value]);
            localStorage.setItem(key, items);
        };

        // проверка: первая запись или нет
        str ? setItem(JSON.parse(str)) : setItem([]);
    },

    //ф-ция обновления значения колличества товаров в корзине
    updateCartAmount: function(){
        let stringArr = localStorage.getItem('idItem');

        let val = JSON.parse(stringArr).length;
        $('.cart-amount').html(val);

    },

    //ф-ция обновления общей стоиости товара в корзине
    updateTotalPrice: function(){
        let stringArr =  localStorage.getItem('prices');
        let arr = JSON.parse(stringArr);
        let result = arr.reduce((sum, current) => sum + current, 0);

        $('.cart-price').html(`${result} грн.`);
        $('.price-value').html(`${result}`);

    },

    //ф-ция получения данных о выбраных товаров из php
    fetchCartProd: async function (url) {
        if(localStorage.getItem('idItem')){
            let promise = await fetch(url,{
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                //отправляем массив выбраных товаров
                body: `array=${localStorage.getItem('idItem')}`
            });
            if(promise.ok){
                $('.list-selected-items').append(await promise.text())
            }
        }else{
            $('<span>empty<span/>').insertAfter($('.customer-form'));
        }
    }
};

$(document).ready(radioStore.initial());

$('.catalog').on('click', event => radioStore.dropdown($(event.currentTarget)));

$('.ph-number-list__ul').on('click', event => radioStore.dropdown($(event.currentTarget)));

$('.nav-list-mobile').on('click', event => radioStore.dropdown($(event.currentTarget)));

$('.cart').on('click', event => {
    radioStore.openCart($(event.currentTarget));
    radioStore.fetchCartProd('php/fetch-cart-prod.php')
});

$('.close-window').on('click', event => {
    radioStore.openCart($(event.currentTarget));
    $('.list-selected-items').empty();
});

$('.customer-form').on('submit', radioStore.makeOrder);

$('.buy-btn').on('click', event => {
    radioStore.itemToLocalStorage('idItem',$(event.currentTarget).attr('value'));
    radioStore.itemToLocalStorage('prices',$(event.currentTarget).attr('priceItem'));
    radioStore.initial()
});

